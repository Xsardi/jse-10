package ru.t1.tbobkov.tm.component;

import ru.t1.tbobkov.tm.api.*;
import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.controller.CommandController;
import ru.t1.tbobkov.tm.controller.ProjectController;
import ru.t1.tbobkov.tm.controller.TaskController;
import ru.t1.tbobkov.tm.repository.CommandRepository;
import ru.t1.tbobkov.tm.repository.ProjectRepository;
import ru.t1.tbobkov.tm.repository.TaskRepository;
import ru.t1.tbobkov.tm.service.CommandService;
import ru.t1.tbobkov.tm.service.ProjectService;
import ru.t1.tbobkov.tm.service.TaskService;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processArgs(final String[] args) {
        if (args == null || args.length < 1) return;
        processArg(args[0]);
        exit();
    }

    private void processCommands() {
        System.out.println("*** TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.printf("\n");
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArg(final String arg) {
        switch (arg.toLowerCase()) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand(final String command) {
        switch (command.toLowerCase()) {
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run(final String... args) {
        processArgs(args);
        processCommands();
    }

}
