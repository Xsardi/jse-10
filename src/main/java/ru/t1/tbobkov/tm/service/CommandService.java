package ru.t1.tbobkov.tm.service;

import ru.t1.tbobkov.tm.api.ICommandRepository;
import ru.t1.tbobkov.tm.api.ICommandService;
import ru.t1.tbobkov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
