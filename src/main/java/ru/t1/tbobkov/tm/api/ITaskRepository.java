package ru.t1.tbobkov.tm.api;

import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

}
