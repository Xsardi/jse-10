package ru.t1.tbobkov.tm.api;

import ru.t1.tbobkov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
