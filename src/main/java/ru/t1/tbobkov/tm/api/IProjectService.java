package ru.t1.tbobkov.tm.api;

import ru.t1.tbobkov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
