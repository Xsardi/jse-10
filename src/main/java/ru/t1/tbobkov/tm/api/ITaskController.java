package ru.t1.tbobkov.tm.api;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
