package ru.t1.tbobkov.tm.api;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showHelp();

    void showSystemInfo();

    void showArgumentError();

    void showCommandError();

}
