package ru.t1.tbobkov.tm.api;

import ru.t1.tbobkov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
