package ru.t1.tbobkov.tm.api;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

}
