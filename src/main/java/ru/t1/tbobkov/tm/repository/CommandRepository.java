package ru.t1.tbobkov.tm.repository;

import ru.t1.tbobkov.tm.api.ICommandRepository;
import ru.t1.tbobkov.tm.constant.ArgumentConstant;
import ru.t1.tbobkov.tm.constant.CommandConstant;
import ru.t1.tbobkov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConstant.HELP, ArgumentConstant.HELP, "show this list of commands");

    private static final Command VERSION = new Command(CommandConstant.VERSION, ArgumentConstant.VERSION, "show version info");

    private static final Command ABOUT = new Command(CommandConstant.ABOUT, ArgumentConstant.ABOUT, "show developer info");

    private static final Command INFO = new Command(CommandConstant.INFO, ArgumentConstant.INFO, "show system info");

    private static final Command EXIT = new Command(CommandConstant.EXIT, null, "close application");

    private static final Command PROJECT_LIST = new Command(CommandConstant.PROJECT_LIST, null, "show project list");

    private static final Command PROJECT_CREATE = new Command(CommandConstant.PROJECT_CREATE, null, "create new project");

    private static final Command PROJECT_CLEAR = new Command(CommandConstant.PROJECT_CLEAR, null, "clear projects");

    private static final Command TASK_LIST = new Command(CommandConstant.TASK_LIST, null, "show task list");

    private static final Command TASK_CREATE = new Command(CommandConstant.TASK_CREATE, null, "create new task");

    private static final Command TASK_CLEAR = new Command(CommandConstant.TASK_CLEAR, null, "clear tasks");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
